//
//  Grid.m
//  Tetris
//
//  Created by Cody Littley on 2/9/15.
//  Copyright (c) 2015 Cody Littley. All rights reserved.
//

#import "Grid.h"

@interface Grid()

@property NSMutableArray* theGrid;

@end

@implementation Grid

-(instancetype) initWithHeight:(unsigned int)height andWidth:(unsigned int)width {
    self = [super init];
    if(self) {
        self.width = width;
        self.height = height;
        
        self.theGrid = [[NSMutableArray alloc] init];
        
        
        //populate the grid with nil
        for(int iii = 0; iii < self.width; iii++) {
            NSMutableArray* newRow = [[NSMutableArray alloc] init];
            for(int jjj = 0; jjj < self.height; jjj++) {
                [newRow addObject:[NSNull null]];
            }
            [self.theGrid addObject:newRow];
        }
    }
    
    
    return self;
}

-(Cell*) getCellAtPosition:(Coordinate*)position {
    if(position.x >= self.width || position.y >= self.height) {
        return nil;
    }
    return self.theGrid[position.x][position.y];
}

-(void) removeCellAtPosition:(Coordinate*)position {
    if(position.x >= self.width || position.y >= self.height) {
        return;
    }
    self.theGrid[position.x][position.y] = [NSNull null];
}

-(BOOL) insertCell:(Cell *)cell atPostion:(Coordinate *)position {
    if(position.x >= self.width || position.x < 0 || position.y >= self.height || position.y < 0) {
        return false;
    }
    
    if([self.theGrid[position.x][position.y] isEqual:[NSNull null]]) {
        cell.position.x = position.x;
        cell.position.y = position.y;
        self.theGrid[position.x][position.y] = cell;
        return true;
    }
    else {
        return false;
    }
}

-(BOOL) isEmptyRelativeToPosition:(Coordinate *)position withOffset:(Coordinate *)offset {
    
    int relativeX = position.x + offset.x;
    int relativeY = position.y + offset.y;
    
    //return false if position is off of the edge
    if(relativeX >= self.width || relativeX < 0 || relativeY >= self.height || relativeY < 0) {
        return false;
    }
    
    if([self.theGrid[relativeX][relativeY] isEqual:[NSNull null]]) {
        return true;
    }
    else {
        return false;
    }
}

-(NSMutableArray*) getFullRows {
    NSMutableArray* rows = [[NSMutableArray alloc] init];
    
    for(int jjj = 0; jjj < self.height; jjj++) {
        BOOL rowIsGood = true;
        for(int iii = 0; iii < self.width; iii++) {
            if([self.theGrid[iii][jjj] isEqual:[NSNull null]]) {
                rowIsGood = false;
                break;
            }
        }
        if(rowIsGood) {
            [rows addObject:@(jjj)];
        }
    }
    
    return rows;
}

-(void) cullRow:(unsigned)row {
    if(row >= self.height) {
        return;
    }
    
    for(int iii = 0; iii < self.width; iii++) {
        for(int jjj = row; jjj < self.height; jjj++) {
            if(![[self getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                if(jjj == self.height - 1) {
                    self.theGrid[iii][jjj] = [NSNull null];
                }
                else if(jjj == row) {
//                    [((Cell*)self.theGrid[iii][jjj]).myView removeFromSuperview];
                    self.theGrid[iii][jjj] = [NSNull null];
                }
                else {
                    Cell* theCell = [self getCellAtPosition:[Coordinate X:iii Y:jjj]];
                    [self removeCellAtPosition:[Coordinate X:iii Y:jjj]];
                    [self insertCell:theCell atPostion:[Coordinate X:iii Y:jjj-1]];
                }
            }
        }
    }
    
}

@end
