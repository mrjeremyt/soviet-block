//
//  ViewController.m
//  Soviet Block
//
//  Created by Jeremy Thompson on 2/18/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "ViewController.h"
#import "AudioController.h"
#import "TetronimoBuilder.h"
#import "Tetronimo.h"
#import "Grid.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *previewBoard;
@property (strong, nonatomic) AudioController *audioController;
@property (strong, nonatomic) Grid *board;
@property (strong, nonatomic) Grid *previewGrid;
@property (strong, nonatomic) Coordinate *insertion;
@property (strong, nonatomic) Coordinate *previewInsertion;
@property (weak, nonatomic) IBOutlet UIView *mainBoardView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) Tetronimo *movingTet;
@property (strong, nonatomic) Tetronimo *previewTet;
@property (strong, nonatomic) Tetronimo *ghostTet;
@property (strong, nonatomic) NSMutableArray *uiViews;
@property (strong, nonatomic) NSMutableArray *uiPreviewViews;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *startButton;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSTimer *t;
@property (weak, nonatomic) IBOutlet UILabel *linesLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (nonatomic) float timerIncrement;
@property (nonatomic) int score;
@property (nonatomic) int linesBusted;
@property (nonatomic) int level;
@property (nonatomic) bool canShiftDown;
@property (nonatomic) bool firstTimeStart;
@property (nonatomic) bool paused;
@property (nonatomic) bool levelCompleteD;
@end

@implementation ViewController
- (IBAction)resetClick:(id)sender {
    [self makeNewGame];
}

- (IBAction)startButtonClick:(id)sender {
    if (self.firstTimeStart) {
        [self.audioController tryPlayMusic];
        [self drop];
        [self updatePreviewUI];
        self.firstTimeStart = false;
        [sender setTitle:@"Pause"];
        return;
    }
    self.paused = !self.paused;
    if (self.paused) {
        [self.timer invalidate];
        [self.audioController audioPlayerBeginInterruption:self.audioController.backgroundMusicPlayer];
        [sender setTitle:@"Start"];
    }else{
        self.timer = [NSTimer scheduledTimerWithTimeInterval:self.timerIncrement target:self selector:@selector(movement) userInfo:nil repeats:true];
        [self.timer fire];
        [self.audioController audioPlayerEndInterruption:self.audioController.backgroundMusicPlayer withOptions:0];
        [sender setTitle:@"Pause"];
    }
}

- (IBAction)swipeRight:(id)sender {
    if(!self.paused && !self.firstTimeStart){
        [self.movingTet shiftRight];
        [self updateUI];
    }
}

- (IBAction)swipeLeft:(id)sender {
    if(!self.paused && !self.firstTimeStart){
        [self.movingTet shiftLeft];
        [self updateUI];
    }
}

- (IBAction)swipeUp:(id)sender {
    if(!self.paused && !self.firstTimeStart){
        [self.movingTet rotateClockwise];
        [self updateUI];
    }
}

- (IBAction)swipeDown:(id)sender {
    if(!self.paused && !self.firstTimeStart){
        bool check = true;
        while(check){
            self.score++;
            check = [self doThings];
        }
        [self updateScores:0];
    }
}


-(bool)checkForEndOfGame{
    bool check1 = [self.board isEmptyRelativeToPosition:[Coordinate X:4 Y:0] withOffset:[Coordinate X:0 Y:20]];
    bool check2 = [self.board isEmptyRelativeToPosition:[Coordinate X:5 Y:0] withOffset:[Coordinate X:0 Y:20]];
    bool check3 = [self.board isEmptyRelativeToPosition:[Coordinate X:6 Y:0] withOffset:[Coordinate X:0 Y:20]];
    bool check4 = [self.board isEmptyRelativeToPosition:[Coordinate X:4 Y:0] withOffset:[Coordinate X:0 Y:21]];
    bool check5 = [self.board isEmptyRelativeToPosition:[Coordinate X:5 Y:0] withOffset:[Coordinate X:0 Y:21]];
    bool check6 = [self.board isEmptyRelativeToPosition:[Coordinate X:6 Y:0] withOffset:[Coordinate X:0 Y:21]];
    
    if (check1 && check2 && check3 && check4 && check5 && check6) {
        return true;
    }
    return false;
}


-(void)addAlertView{
    [self.audioController audioPlayerBeginInterruption:self.audioController.backgroundMusicPlayer];
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Game Over"
                                                       message:@"You have lost the Soviet Block. Press Ok to start a new game."
                                                      delegate:self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
    [alertView setTag:0];
    [alertView show];
}

-(void)levelComplete{
    [self.timer invalidate];
    if(!self.levelCompleteD){
        self.levelCompleteD = true;
        [self.audioController audioPlayerBeginInterruption:self.audioController.backgroundMusicPlayer];
        UIAlertView *nextLevel = [[UIAlertView alloc]initWithTitle:@"Level Complete"
                                                           message:[NSString stringWithFormat:@"You have completed level %d. Press ok to go to the next level or try again", self.level]
                                                          delegate:self
                                                 cancelButtonTitle:@"Try Again"
                                                 otherButtonTitles:@"Ok", nil];
        [nextLevel setTag:1];
        [nextLevel show];
    }else{
        return;
    }
}

-(bool)doThings{
    bool temp = [self.movingTet shiftDown];
    self.canShiftDown = temp;
    if (self.canShiftDown == false) {
        [self.timer invalidate];
        [self cull];
        if([self checkForEndOfGame]){
            [self drop];
        }else{ [self addAlertView]; }
    }
    [self updateUI];
    return temp;
}

-(void)cull{
    NSMutableArray *rows = [[NSMutableArray alloc] init];
    rows = [self.board getFullRows];
    int count = (int)[rows count];
    if(count < 1) return;
    else{
        self.backgroundView.backgroundColor = [self randomColor];
        for(int i = count - 1; i >= 0; i--){
            int index = [(NSNumber*)[rows objectAtIndex:i] intValue];
            [self.board cullRow:index]; self.linesBusted++;
        }
    }
    [self updateScores:count];
    [self.t fire];
}

-(void)resetBackground:(NSTimer *)timer{
    self.backgroundView.backgroundColor = [self thisIsTheLastStrawCody:@"black"];
    [timer invalidate];
}

-(void)updateScores:(int) numLines{
    if(numLines == 0){
        self.score += 0;
    }else if(numLines == 1){
        self.score += 40*self.level;
    }else if(numLines == 2){
        self.score += 100*self.level;
    }else if(numLines == 3){
        self.score += 300*self.level;
    }else if(numLines == 4){
        self.score += 1200*self.level;
    }else{
        self.score += 1300*self.level;
    }
    [self.scoreLabel setText:[NSString stringWithFormat:@"%d", self.score]];
    if(self.linesBusted >= 12){
        [self levelComplete];
    }
}

-(NSMutableArray *)uiViews{
    if(!_uiViews){
        _uiViews = [[NSMutableArray alloc] init];
    }
    return _uiViews;
}

-(NSMutableArray *)uiPreviewViews{
    if(!_uiPreviewViews){
        _uiPreviewViews = [[NSMutableArray alloc] init];
    }
    return _uiPreviewViews;
}

-(void)movement{
    [self doThings];
}

//-(void)ghost{
//    [self.ghostTet insertIntoGrid:self.board atPosition:self.insertion];
//    bool check = true;
//    while(check){
//        check = [self.ghostTet shiftDown];
//        if (check == false) {
//
//        }
//        [self updateSpecialUI];
//    }
//}

//-(void)updateSpecialUI{
//    for (int i = self.ghostTet.mapping.height - 1; i >= 0; i--) {
//        for(int j = 0; j < self.ghostTet.mapping.width; j++){
//            Cell *c = [self.ghostTet.mapping getCellAtPosition:[Coordinate X:j Y:i]];
//            NSLog(@"what: %@", c);
//            if (![c isEqual:[NSNull null]]) {
//                Coordinate *co = c.position;
//                NSLog(@"x is: %d, Y is: %d", co.x, co.y);
//                UIView *v = [self.uiViews objectAtIndex:(((19 - (co.y-1))*10) + co.x)];
//                v.backgroundColor = self.mainBoardView.backgroundColor;
//                v.layer.borderColor = [UIColor blackColor].CGColor;
//                v.layer.borderWidth = 1.0f;
//            }
//        }
//    }
//}


-(void)drop{
    self.movingTet = self.previewTet;
    self.previewTet = [TetronimoBuilder buildRandomTetronimo];
    [self thisIsLame];
    self.ghostTet = self.movingTet;
//    [self ghost];
    [self.movingTet insertIntoGrid:self.board atPosition:self.insertion];
    [self.previewTet insertIntoGrid:self.previewGrid atPosition:self.previewInsertion];
    [self updatePreviewUI];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.timerIncrement target:self selector:@selector(movement) userInfo:nil repeats:true];
    [self.timer fire];
}

-(UIColor *)randomColor{
    switch (arc4random()%5) {
        case 0: return [UIColor greenColor];
        case 1: return [UIColor blueColor];
        case 2: return [UIColor orangeColor];
        case 3: return [UIColor redColor];
        case 4: return [UIColor purpleColor];
    }
    return [UIColor blackColor];
}

-(UIColor *)thisIsTheLastStrawCody: (NSString *)whyIsThisAString{
    
    if ([whyIsThisAString isEqualToString:@"cyan"]) { return [UIColor cyanColor]; }
    else if ([whyIsThisAString isEqualToString:@"blue"]) { return [UIColor blueColor]; }
    else if ([whyIsThisAString isEqualToString:@"orange"]) { return [UIColor orangeColor]; }
    else if ([whyIsThisAString isEqualToString:@"yellow"]) { return [UIColor yellowColor]; }
    else if ([whyIsThisAString isEqualToString:@"green"]) { return [UIColor greenColor]; }
    else if ([whyIsThisAString isEqualToString:@"purple"]) { return [UIColor purpleColor]; }
    else if ([whyIsThisAString isEqualToString:@"red"]) { return [UIColor redColor]; }
    else{ return [UIColor blackColor]; }
    
}

-(void)updateUI{
    
    for (int i = self.board.height-3; i >= 0; i--) {
        for(int j = 0; j < self.board.width; j++){
            Cell *c = [self.board getCellAtPosition:[Coordinate X:j Y:i]];
            UIView *v = [self.uiViews objectAtIndex:(((19 - i)*10) + j)];
            v.layer.borderWidth = 0.0;
            v.backgroundColor = self.mainBoardView.backgroundColor;
            if (![c isEqual:[NSNull null]]) {
                v.backgroundColor = [self thisIsTheLastStrawCody:c.color];
                v.layer.borderColor = [UIColor blackColor].CGColor;
                v.layer.borderWidth = 1.0f;
            }
        }
    }
//    [self updateSpecialUI];
    [self.linesLabel setText:[NSString stringWithFormat:@"%d", self.linesBusted]];
}

-(void)updatePreviewUI{
    for (int i = self.previewGrid.height-1; i >= 0; i--) {
        for(int j = 0; j < self.previewGrid.width; j++){
            Cell *c = [self.previewGrid getCellAtPosition:[Coordinate X:j Y:i]];
            UIView *v = [self.uiPreviewViews objectAtIndex:(((3 - i)*4) + j)];
            v.layer.borderWidth = 0.0;
            v.backgroundColor = self.previewBoard.backgroundColor;
            if (![c isEqual:[NSNull null]]) {
                v.backgroundColor = [self thisIsTheLastStrawCody:c.color];
                v.layer.borderColor = [UIColor blackColor].CGColor;
                v.layer.borderWidth = 1.0f;
            }
        }
    }
}

-(void)thisIsLame{
    self.previewGrid = [[Grid alloc] initWithHeight:4 andWidth:4];
}

-(void)initializeUIBoard{
    CGSize CELL_SIZE = {24, 24};
    
    for(int i = 0; i < 20; i++){
        for(int j = 0; j < 10; j++){
            CGRect frame;
            frame.size = CELL_SIZE;
            frame.origin.x = j * (CELL_SIZE.width);
            frame.origin.y = i * (CELL_SIZE.height);
            UIView *v = [[UIView alloc]initWithFrame:frame];
            v.backgroundColor = self.mainBoardView.backgroundColor;
            [self.mainBoardView addSubview:v];
            [self.uiViews addObject:v];
        }
    }
}

-(void)initializePreviewBoard{
    CGSize CELL_SIZE = {15, 15};
    
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            CGRect frame;
            frame.size = CELL_SIZE;
            frame.origin.x = j * (CELL_SIZE.width);
            frame.origin.y = i * (CELL_SIZE.height);
            UIView *v = [[UIView alloc]initWithFrame:frame];
            v.backgroundColor = self.previewBoard.backgroundColor;
            [self.previewBoard addSubview:v];
            [self.uiPreviewViews addObject:v];
        }
    }
}


-(void)nextLevel{
    self.level++;
    self.timerIncrement -= .002;
    [self makeNewGame];
}

-(void)makeNewGame{
    self.board = [[Grid alloc] initWithHeight:22 andWidth:10];
    self.previewGrid = [[Grid alloc] initWithHeight:4 andWidth:4];
    self.firstTimeStart = true;
    self.paused = false;
    self.linesBusted = 0;
    self.score = 0;
    [self updateUI];
    self.previewTet = [TetronimoBuilder buildRandomTetronimo];
    [self.previewTet insertIntoGrid:self.previewGrid atPosition:self.previewInsertion];
    [self updatePreviewUI];
    [self.levelLabel setText:[NSString stringWithFormat:@"%d", self.level]];
    [self.scoreLabel setText:[NSString stringWithFormat:@"%d", self.score]];
    [self.startButton setTitle:@"Start"];
    self.levelCompleteD = false;
    [self.timer invalidate];
    self.backgroundView.backgroundColor = [self thisIsTheLastStrawCody:@"black"];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.audioController = [[AudioController alloc] init];
    [self initializeUIBoard];
    [self initializePreviewBoard];
    _board = [[Grid alloc] initWithHeight:22 andWidth:10];
    _previewGrid = [[Grid alloc] initWithHeight:4 andWidth:4];
    _insertion = [Coordinate X:4 Y:22];
    _previewInsertion = [Coordinate X:0 Y:4];
    _firstTimeStart = true;
    _paused = false;
    _previewTet = [TetronimoBuilder buildRandomTetronimo];
    [_previewTet insertIntoGrid:self.previewGrid atPosition:self.previewInsertion];
    [self updatePreviewUI];
    _linesBusted = 0;
    _score = 0;
    _level = 1;
    [_levelLabel setText:[NSString stringWithFormat:@"%d", _level]];
    _timerIncrement = 0.5;
    _levelCompleteD = false;
    _t = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(resetBackground:) userInfo:nil repeats:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:
(NSInteger)buttonIndex{
    if(alertView.tag == 0){
        switch (buttonIndex) {
            case 0:
                [self.audioController audioPlayerEndInterruption:self.audioController.backgroundMusicPlayer withOptions:0];
                [self makeNewGame];
                [self.startButton setTitle:@"Start"];
                break;
        }
        
    }else{
        switch (buttonIndex) {
            case 0:
                [self makeNewGame];
                [self.startButton setTitle:@"Start"];
                break;
            case 1:
                [self.audioController audioPlayerEndInterruption:self.audioController.backgroundMusicPlayer withOptions:0];
                [self nextLevel];
                break;
                
            default:
                break;
        }
    }
}

@end