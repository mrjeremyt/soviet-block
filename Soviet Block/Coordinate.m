//
//  Coordinate.m
//  Tetris
//
//  Created by user on 2/10/15.
//  Copyright (c) 2015 Cody Littley. All rights reserved.
//

#import "Coordinate.h"

@implementation Coordinate

+(Coordinate*) X:(int)x Y:(int)y {
    Coordinate* tmp = [[Coordinate alloc] init];
    tmp.x = x;
    tmp.y = y;
    return tmp;
}

@end
