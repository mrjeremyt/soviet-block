//
//  Coordinate.h
//  Tetris
//
//  Created by user on 2/10/15.
//  Copyright (c) 2015 Cody Littley. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Coordinate : NSObject
@property (nonatomic) int x;
@property (nonatomic) int y;
+(Coordinate*) X:(int)x Y:(int)y;
@end
