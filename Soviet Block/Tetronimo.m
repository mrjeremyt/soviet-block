//
//  Tetronimo.m
//  Tetris
//
//  Created by Cody Littley on 2/9/15.
//  Copyright (c) 2015 Cody Littley. All rights reserved.
//

#import "Tetronimo.h"

@implementation Tetronimo

-(instancetype) initWithMap:(Grid *)mapping {
    self = [super init];
    if(self) {
        self.mapping = mapping;
    }
    return self;
}

-(BOOL) insertIntoGrid:(Grid *)grid atPosition:(Coordinate*)position {
    
    self.parent = grid;
    Coordinate* oldPosition = self.position;
    self.position = position;
    
    int xCorrection = position.x;
    int yCorrection = position.y - self.mapping.height;
    
    //check to see if insertion is legal
    BOOL isGood = true;
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                if(![self.parent isEmptyRelativeToPosition:[Coordinate X:iii+xCorrection Y:jjj+yCorrection]
                                                withOffset:[Coordinate X:0 Y:0]]) {
                    isGood = false;
                    break;
                }
            }
        }
    }
    
    if(!isGood) {
        self.position = oldPosition;
        return false;
    }
    
    //actually insert the tetraminio
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                [grid insertCell:[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]]
                       atPostion:[Coordinate X:iii+xCorrection Y:jjj+yCorrection]];
            }
        }
    }

    return true;
}

-(BOOL) shiftDown {
    
    //remove tetronimo from parent grid
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                [self.parent removeCellAtPosition:[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]].position];
            }
        }
    }
    
    BOOL success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x Y:self.position.y-1]];

    if(success) {
        return true;
    }
    else {    //failed, put it back
        [self insertIntoGrid:self.parent atPosition:self.position];
        return false;
    }
    
}

-(BOOL) shiftLeft {
    
    //remove tetronimo from parent grid
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                [self.parent removeCellAtPosition:[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]].position];
            }
        }
    }
    
    BOOL success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x-1 Y:self.position.y]];
    
    if(success) {
        return true;
    }
    else {    //failed, put it back
        [self insertIntoGrid:self.parent atPosition:self.position];
        return false;
    }
    
}

-(BOOL) shiftRight {
    
    //remove tetronimo from parent grid
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                [self.parent removeCellAtPosition:[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]].position];
            }
        }
    }
    
    BOOL success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x+1 Y:self.position.y]];
    
    if(success) {
        return true;
    }
    else {    //failed, put it back
        [self insertIntoGrid:self.parent atPosition:self.position];
        return false;
    }
    
}

-(BOOL) rotateCounterClockwise {
    
    //special case, mainly used in the "O" type block
    if(self.mapping.width != self.mapping.height) {
        return true;
    }
    
    //remove tetronimo from parent grid
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                [self.parent removeCellAtPosition:[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]].position];
            }
        }
    }
    
    //take the transpose of the mapping
    Grid* transposeGrid = [[Grid alloc] initWithHeight:self.mapping.height andWidth:self.mapping.height];
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                [transposeGrid insertCell:[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]]
                                atPostion:[Coordinate X:jjj Y:iii]];
            }
        }
    }
    
    //reverse the rows in transpose grid to get rotation by -90 degrees
    Grid* rotatedGrid = [[Grid alloc] initWithHeight:self.mapping.height andWidth:self.mapping.height];
    float center = (self.mapping.width-1) / 2.0;
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[transposeGrid getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                int newX = -1;
                if(iii == center) {
                    newX = iii;
                }
                else if(iii > center) {
                    newX = iii - (self.mapping.height - 1) + 2*(self.mapping.height - 1 - iii);
                }
                else if(iii < center) {
                    newX = self.mapping.height - iii - 1;
                }
                
                //NSLog(@"Old coordinates: %d, %d", iii,jjj);
                //NSLog(@"New coordinates: %d, %d\n\n---", iii, newX);
                
                [rotatedGrid insertCell:[transposeGrid getCellAtPosition:[Coordinate X:iii Y:jjj]]
                              atPostion:[Coordinate X:newX Y:jjj]];
            }
        }
    }
    
    
    Grid* oldLayout = self.mapping;
    self.mapping = rotatedGrid;
    
    BOOL success = [self insertIntoGrid:self.parent atPosition:self.position];
    if(success) {
        return true;
    }
    
    //attempt wall kick to the left   http://tetris.wikia.com/wiki/Wall_kick
    success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x - 1 Y:self.position.y]];
    if(success) {
        return true;
    }
    
    //Attempt super wall kick left if it is a 4x4 tetronimo
    if(self.mapping.width >= 4) {
        success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x - 2 Y:self.position.y]];
        if(success) {
            return true;
        }
    }
    
    //attempt wall kick to the right   http://tetris.wikia.com/wiki/Wall_kick
    success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x + 1 Y:self.position.y]];
    if(success) {
        return true;
    }
    
    //Attempt super wall kick right if it is a 4x4 tetronimo
    if(self.mapping.width >= 4) {
        success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x + 2 Y:self.position.y]];
        if(success) {
            return true;
        }
    }
    
    //failed, put it back
    self.mapping = oldLayout;
    [self insertIntoGrid:self.parent atPosition:self.position];
    return false;
    
}

-(BOOL) rotateClockwise {
    
    //special case, mainly used in the "O" type block
    if(self.mapping.width != self.mapping.height) {
        return true;
    }
    
    //remove tetronimo from parent grid
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                [self.parent removeCellAtPosition:[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]].position];
            }
        }
    }
    
    //take the transpose of the mapping
    Grid* transposeGrid = [[Grid alloc] initWithHeight:self.mapping.height andWidth:self.mapping.height];
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                [transposeGrid insertCell:[self.mapping getCellAtPosition:[Coordinate X:iii Y:jjj]]
                                atPostion:[Coordinate X:jjj Y:iii]];
            }
        }
    }
    
    //reverse the columns in transpose grid to get rotation by 90 degrees
    Grid* rotatedGrid = [[Grid alloc] initWithHeight:self.mapping.height andWidth:self.mapping.height];
    float center = (self.mapping.height-1) / 2.0;
    for(int iii = 0; iii < self.mapping.width; iii++) {
        for(int jjj = 0; jjj < self.mapping.height; jjj++) {
            if(![[transposeGrid getCellAtPosition:[Coordinate X:iii Y:jjj]] isEqual:[NSNull null]]) {
                int newY = -1;
                if(jjj == center) {
                    newY = jjj;
                }
                else if(jjj > center) {
                    newY = jjj - (self.mapping.height - 1) + 2*(self.mapping.height - 1 - jjj);
                }
                else if(jjj < center) {
                    newY = self.mapping.height - jjj - 1;
                }
                
                //NSLog(@"Old coordinates: %d, %d", iii,jjj);
                //NSLog(@"New coordinates: %d, %d\n\n---", iii, newY);
                
                [rotatedGrid insertCell:[transposeGrid getCellAtPosition:[Coordinate X:iii Y:jjj]]
                              atPostion:[Coordinate X:iii Y:newY]];
            }
        }
    }
    
    
    Grid* oldLayout = self.mapping;
    self.mapping = rotatedGrid;
    
    BOOL success = [self insertIntoGrid:self.parent atPosition:self.position];
    if(success) {
        return true;
    }
    
    //attempt wall kick to the left   http://tetris.wikia.com/wiki/Wall_kick
    success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x - 1 Y:self.position.y]];
    if(success) {
        return true;
    }
    
    //Attempt super wall kick left if it is a 4x4 tetronimo
    if(self.mapping.width >= 4) {
        success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x - 2 Y:self.position.y]];
        if(success) {
            return true;
        }
    }
    
    //attempt wall kick to the right   http://tetris.wikia.com/wiki/Wall_kick
    success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x + 1 Y:self.position.y]];
    if(success) {
        return true;
    }
    
    //Attempt super wall kick right if it is a 4x4 tetronimo
    if(self.mapping.width >= 4) {
        success = [self insertIntoGrid:self.parent atPosition:[Coordinate X:self.position.x + 2 Y:self.position.y]];
        if(success) {
            return true;
        }
    }
    
    //failed, put it back
    self.mapping = oldLayout;
    [self insertIntoGrid:self.parent atPosition:self.position];
    return false;
    
}

@end
