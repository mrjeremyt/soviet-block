//
//  TetronimoBuilder.h
//  Tetris
//
//  Created by user on 2/10/15.
//  Copyright (c) 2015 Cody Littley. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Tetronimo.h"

@interface TetronimoBuilder : NSObject

+(NSArray*) types;

+(Tetronimo*) buildRandomTetronimo;

+(Tetronimo*) buildTetronimoType:(NSString*)name;

+(Tetronimo*) tet_i;
+(Tetronimo*) tet_j;
+(Tetronimo*) tet_l;
+(Tetronimo*) tet_o;
+(Tetronimo*) tet_s;
+(Tetronimo*) tet_t;
+(Tetronimo*) tet_z;

@end
