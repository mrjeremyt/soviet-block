//
//  Tetronimo.h
//  Tetris
//
//  Created by Cody Littley on 2/9/15.
//  Copyright (c) 2015 Cody Littley. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Coordinate.h"
#import "Grid.h"
@class Grid;

//See http://tetris.wikia.com/wiki/Tetromino

@interface Tetronimo : NSObject

@property (strong, nonatomic) Grid* mapping;
@property (strong, nonatomic) Coordinate* position;
@property (weak, nonatomic) Grid* parent;

//returns true if motion has happened
-(BOOL) rotateClockwise;
-(BOOL) rotateCounterClockwise;
-(BOOL) shiftDown;
-(BOOL) shiftLeft;
-(BOOL) shiftRight;

//mapping should be a grid populated with cells
//see http://tetris.wikia.com/wiki/SRS
-(instancetype) initWithMap:(Grid*)mapping;

//Insert the tetronimo into a grid object
//position is the place where the upper left corner of the mapping goes
//returns false if unable to insert
-(BOOL) insertIntoGrid:(Grid*)grid atPosition:(Coordinate*)position;

@end
