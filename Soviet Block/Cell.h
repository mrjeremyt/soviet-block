//
//  Cell.h
//  Tetris
//
//  Created by Cody Littley on 2/9/15.
//  Copyright (c) 2015 Cody Littley. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "Tetronimo.h"
#import "Coordinate.h"

@interface Cell : NSObject

@property (strong, nonatomic) NSString* color;

//Points to the coorisponding view that represents this cell.
//Class Cell doesn't actually modify/generate this view,  It just points to it for the sake of convience.
//Maybe a violaion of MVC style, but it is very eligant/efficient to do it this way.
@property (strong, nonatomic) UIView* myView;

@property (strong, nonatomic) Coordinate* position; //useful for backwards lookup in a grid

-(instancetype) initWithColor:(NSString*)color;

@end
