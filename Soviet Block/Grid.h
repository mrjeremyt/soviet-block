//
//  Grid.h
//  Tetris
//
//  Created by Cody Littley on 2/9/15.
//  Copyright (c) 2015 Cody Littley. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Cell.h"
@class Cell; //to shut up the compiler

#import "Coordinate.h"

@interface Grid : NSObject

@property (nonatomic) unsigned height;
@property (nonatomic) unsigned width;

-(instancetype) initWithHeight:(unsigned)height andWidth:(unsigned)width;

-(Cell*) getCellAtPosition:(Coordinate*)position;

-(void) removeCellAtPosition:(Coordinate*)position;

//return false if unable to insert cell at position
-(BOOL) insertCell:(Cell*)cell atPostion:(Coordinate*)position;

//given a cell and a relative offset in the X and Y direction, return true if there is nothing in that space
//if a space at the edge of the grid is requested return false
-(BOOL) isEmptyRelativeToPosition:(Coordinate*)position withOffset:(Coordinate*)offset;

//detect if there are any rows that are full.  Return
//a list of the rows that are full
-(NSMutableArray*) getFullRows;

//delete a given row
//shift everything down by one position
-(void) cullRow:(unsigned)row ;

@end
