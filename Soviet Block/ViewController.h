//
//  ViewController.h
//  Soviet Block
//
//  Created by Jeremy Thompson on 2/18/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ViewController : UIViewController<UIAlertViewDelegate>


@end

