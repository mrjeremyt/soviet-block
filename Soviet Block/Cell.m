//
//  Cell.m
//  Tetris
//
//  Created by Cody Littley on 2/9/15.
//  Copyright (c) 2015 Cody Littley. All rights reserved.
//

#import "Cell.h"

@implementation Cell

-(instancetype) initWithColor:(NSString *)color {
    self = [super init];
    if(self) {
        self.position = [Coordinate X:-1 Y:-1];
        self.color = color;
    }
    return self;
}

@end
