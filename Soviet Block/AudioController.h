//
//  AudioController.h
//  Soviet Block
//
//  Created by Jeremy Thompson on 2/19/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AVFoundation;

@interface AudioController : NSObject

- (instancetype)init;
- (void)tryPlayMusic;
- (void)playSystemSound;
@property (strong, nonatomic) AVAudioPlayer *backgroundMusicPlayer;
- (void) audioPlayerBeginInterruption: (AVAudioPlayer *) player;
- (void) audioPlayerEndInterruption: (AVAudioPlayer *) player withOptions:(NSUInteger) flags;

@end
