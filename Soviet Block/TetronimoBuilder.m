//
//  TetronimoBuilder.m
//  Tetris
//
//  Created by user on 2/10/15.
//  Copyright (c) 2015 Cody Littley. All rights reserved.
//

#import "TetronimoBuilder.h"

#import "Cell.h"
#import "Coordinate.h"

@interface Tetronimo()


@end

@implementation TetronimoBuilder

+(NSArray*) types {
    return @[@"i",@"j",@"l",@"o",@"s",@"t",@"z"];
}

+(Tetronimo*) buildTetronimoType:(NSString *)name {
    
    if([name isEqualToString:@"i"]) {
        return [TetronimoBuilder tet_i];
    }
    else if([name isEqualToString:@"j"]) {
       return [TetronimoBuilder tet_j];
    }
    else if([name isEqualToString:@"l"]) {
       return [TetronimoBuilder tet_l];
    }
    else if([name isEqualToString:@"o"]) {
        return [TetronimoBuilder tet_o];
    }
    else if([name isEqualToString:@"s"]) {
        return [TetronimoBuilder tet_s];
    }
    else if([name isEqualToString:@"t"]) {
        return [TetronimoBuilder tet_t];
    }
    else if([name isEqualToString:@"z"]) {
        return [TetronimoBuilder tet_z];
    }
    
    return nil;
}

+(Tetronimo*) tet_i {
    Grid* newGrid = [[Grid alloc] initWithHeight:4 andWidth:4];
    NSString* color = @"cyan";
    Cell* cell1 = [[Cell alloc] initWithColor:color];
    Cell* cell2 = [[Cell alloc] initWithColor:color];
    Cell* cell3 = [[Cell alloc] initWithColor:color];
    Cell* cell4 = [[Cell alloc] initWithColor:color];
    [newGrid insertCell:cell1 atPostion:[Coordinate X:0 Y:2]];
    [newGrid insertCell:cell2 atPostion:[Coordinate X:1 Y:2]];
    [newGrid insertCell:cell3 atPostion:[Coordinate X:2 Y:2]];
    [newGrid insertCell:cell4 atPostion:[Coordinate X:3 Y:2]];
    Tetronimo* result = [[Tetronimo alloc] initWithMap:newGrid];
    return result;
}

+(Tetronimo*) tet_j {
    Grid* newGrid = [[Grid alloc] initWithHeight:3 andWidth:3];
    NSString* color = @"blue";
    Cell* cell1 = [[Cell alloc] initWithColor:color];
    Cell* cell2 = [[Cell alloc] initWithColor:color];
    Cell* cell3 = [[Cell alloc] initWithColor:color];
    Cell* cell4 = [[Cell alloc] initWithColor:color];
    [newGrid insertCell:cell1 atPostion:[Coordinate X:0 Y:2]];
    [newGrid insertCell:cell2 atPostion:[Coordinate X:0 Y:1]];
    [newGrid insertCell:cell3 atPostion:[Coordinate X:1 Y:1]];
    [newGrid insertCell:cell4 atPostion:[Coordinate X:2 Y:1]];
    Tetronimo* result = [[Tetronimo alloc] initWithMap:newGrid];
    return result;
}

+(Tetronimo*) tet_l {
    Grid* newGrid = [[Grid alloc] initWithHeight:3 andWidth:3];
    NSString* color = @"orange";
    Cell* cell1 = [[Cell alloc] initWithColor:color];
    Cell* cell2 = [[Cell alloc] initWithColor:color];
    Cell* cell3 = [[Cell alloc] initWithColor:color];
    Cell* cell4 = [[Cell alloc] initWithColor:color];
    [newGrid insertCell:cell1 atPostion:[Coordinate X:2 Y:2]];
    [newGrid insertCell:cell2 atPostion:[Coordinate X:0 Y:1]];
    [newGrid insertCell:cell3 atPostion:[Coordinate X:1 Y:1]];
    [newGrid insertCell:cell4 atPostion:[Coordinate X:2 Y:1]];
    Tetronimo* result = [[Tetronimo alloc] initWithMap:newGrid];
    return result;
}

+(Tetronimo*) tet_o {
    Grid* newGrid = [[Grid alloc] initWithHeight:3 andWidth:4];
    NSString* color = @"yellow";
    Cell* cell1 = [[Cell alloc] initWithColor:color];
    Cell* cell2 = [[Cell alloc] initWithColor:color];
    Cell* cell3 = [[Cell alloc] initWithColor:color];
    Cell* cell4 = [[Cell alloc] initWithColor:color];
    [newGrid insertCell:cell1 atPostion:[Coordinate X:1 Y:1]];
    [newGrid insertCell:cell2 atPostion:[Coordinate X:1 Y:2]];
    [newGrid insertCell:cell3 atPostion:[Coordinate X:2 Y:1]];
    [newGrid insertCell:cell4 atPostion:[Coordinate X:2 Y:2]];
    Tetronimo* result = [[Tetronimo alloc] initWithMap:newGrid];
    return result;
}

+(Tetronimo*) tet_s {
    Grid* newGrid = [[Grid alloc] initWithHeight:3 andWidth:3];
    NSString* color = @"green";
    Cell* cell1 = [[Cell alloc] initWithColor:color];
    Cell* cell2 = [[Cell alloc] initWithColor:color];
    Cell* cell3 = [[Cell alloc] initWithColor:color];
    Cell* cell4 = [[Cell alloc] initWithColor:color];
    [newGrid insertCell:cell1 atPostion:[Coordinate X:0 Y:1]];
    [newGrid insertCell:cell2 atPostion:[Coordinate X:1 Y:1]];
    [newGrid insertCell:cell3 atPostion:[Coordinate X:1 Y:2]];
    [newGrid insertCell:cell4 atPostion:[Coordinate X:2 Y:2]];
    Tetronimo* result = [[Tetronimo alloc] initWithMap:newGrid];
    return result;
}

+(Tetronimo*) tet_t {
    Grid* newGrid = [[Grid alloc] initWithHeight:3 andWidth:3];
    NSString* color = @"purple";
    Cell* cell1 = [[Cell alloc] initWithColor:color];
    Cell* cell2 = [[Cell alloc] initWithColor:color];
    Cell* cell3 = [[Cell alloc] initWithColor:color];
    Cell* cell4 = [[Cell alloc] initWithColor:color];
    [newGrid insertCell:cell1 atPostion:[Coordinate X:0 Y:1]];
    [newGrid insertCell:cell2 atPostion:[Coordinate X:1 Y:1]];
    [newGrid insertCell:cell3 atPostion:[Coordinate X:2 Y:1]];
    [newGrid insertCell:cell4 atPostion:[Coordinate X:1 Y:2]];
    Tetronimo* result = [[Tetronimo alloc] initWithMap:newGrid];
    return result;
}

+(Tetronimo*) tet_z {
    Grid* newGrid = [[Grid alloc] initWithHeight:3 andWidth:3];
    NSString* color = @"red";
    Cell* cell1 = [[Cell alloc] initWithColor:color];
    Cell* cell2 = [[Cell alloc] initWithColor:color];
    Cell* cell3 = [[Cell alloc] initWithColor:color];
    Cell* cell4 = [[Cell alloc] initWithColor:color];
    [newGrid insertCell:cell1 atPostion:[Coordinate X:0 Y:2]];
    [newGrid insertCell:cell2 atPostion:[Coordinate X:1 Y:2]];
    [newGrid insertCell:cell3 atPostion:[Coordinate X:1 Y:1]];
    [newGrid insertCell:cell4 atPostion:[Coordinate X:2 Y:1]];
    Tetronimo* result = [[Tetronimo alloc] initWithMap:newGrid];
    return result;
}

+(Tetronimo*) buildRandomTetronimo {
    NSString* type = [TetronimoBuilder types][arc4random_uniform(7)];
    return [TetronimoBuilder buildTetronimoType:type];
}
@end
